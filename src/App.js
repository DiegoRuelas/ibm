import React from 'react';
import logo from './logo.svg';
import './App.css';
import Access from './Access';
import {Button} from 'react-bootstrap';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Access/>
      </header>
    </div>
  );
}

export default App
